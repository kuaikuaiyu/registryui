#!/usr/bin/env python
# -*- coding: utf8 -*-
from tornado import gen
from configure import ALIYUN_OSS_AK
from configure import ALIYUN_OSS_PK
from configure import ALIYUN_OSS_BUCKET
from configure import ALIYUN_OSS_ENDPOINT
import utils
import logging
import oss2


auth = oss2.Auth(ALIYUN_OSS_AK, ALIYUN_OSS_PK)
bucket = oss2.Bucket(auth, ALIYUN_OSS_ENDPOINT, ALIYUN_OSS_BUCKET)

namespace_prefix = "docker/registry/v2/repositories/"

@gen.coroutine
def fetch_namespace_dirs():
    dirs = []
    marker = ''
    while True:
        response = yield bucket.list_objects(
            namespace_prefix, "/", marker=marker)
        dirs += response.prefix_list
        if not response.is_truncated:
            break
        marker = response.next_marker
    raise gen.Return(dirs)


@gen.coroutine
def fetch_images():
    dirs = yield fetch_namespace_dirs()
    @gen.coroutine
    def fn(d, callback_):
        if not d.endswith('/'):
            d += "/"
        image_dirs = []
        marker = ''
        while True:
            response = yield bucket.list_objects(d, "/", marker=marker)
            image_dirs += response.prefix_list
            if not response.is_truncated:
                break
            marker = response.next_marker
        callback_(image_dirs)
    image_dir_parts = yield gen.Task(utils.async_map, fn, dirs)
    image_dirs = []
    for idp in image_dir_parts:
        image_dirs += idp
    images = [d[len(namespace_prefix):-1] for d in image_dirs]
    raise gen.Return(images)

@gen.coroutine
def fetch_image_tags(image):
    tags_common_suffix = "/_manifests/tags/"
    tags_prefix = "%s%s%s" % (namespace_prefix, image, tags_common_suffix)
    raw_tags = []
    marker = ''
    while True:
        response = yield bucket.list_objects(tags_prefix, "/", marker=marker)
        dirs = response.prefix_list
        raw_tags += [d[len(tags_prefix):] for d in dirs]
        if not response.is_truncated:
            break
        marker = response.next_marker
    tags = [{'tag': rt if not rt.endswith("/") else rt[0:-1], 'hash': ''} for rt in raw_tags]
    raise gen.Return(tags)

@gen.coroutine
def fetch_image_tag_hash(image, tag):
    raise NotImplementedError()


@gen.coroutine
def fetch_registry_info():
    images = yield fetch_images()
    @gen.coroutine
    def fn(image, callback_):
        tags = yield fetch_image_tags(image)
        retval = {
            "name": image,
            "tags": tags
        }
        callback_(retval)
    results = yield gen.Task(utils.async_map, fn, images)
    retval = {}
    for result in results:
        retval[result["name"]] = result["tags"]
    raise gen.Return(retval)
