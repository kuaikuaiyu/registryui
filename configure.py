#!/usr/bin/env python
# -*- coding: utf8 -*-
import os
import logging

THIS_DIR = os.path.abspath(os.path.dirname(__file__))


def s2b(value):
    if value.upper() == "FALSE":
        return False
    else:
        return True

def log_level_name_to_log_level(name):
    name = name.upper()
    if name == "DEBUG":
        return logging.DEBUG
    elif name == "INFO":
        return logging.INFO
    elif name == "WARN":
        return logging.WARN
    elif name == "ERROR":
        return logging.ERROR
    elif name == "CRITICAL":
        return logging.CRITICAL
    elif name == "NOTSET":
        return logging.NOTSET
    else:
        logging.error("invalid logging level name %s, set to NOTSET", name)
        return logging.NOTSET

DEBUG = s2b(os.getenv('DEBUG', 'true'))
REDIRECT_URL = os.getenv('REDIRECT_URL', 'https://kuaikuaiyu.com')
SERVER_PORT = int(os.getenv('SERVER_PORT', '8080'))
SERVER_ADDRESS = os.getenv('SERVER_ADDRESS', '0.0.0.0')
LOG_LEVEL = log_level_name_to_log_level(os.getenv("LOG_LEVEL", "debug"))
LOG_FORMAT = "[%(levelname)s %(asctime)s %(pathname)s:%(lineno)d] %(message)s"

TEMPLATE_PATH = os.path.join(THIS_DIR, "template")

ALIYUN_OSS_AK = os.getenv("ALIYUN_OSS_AK")
ALIYUN_OSS_PK = os.getenv("ALIYUN_OSS_PK")
ALIYUN_OSS_ENDPOINT = os.getenv("ALIYUN_OSS_ENDPOINT")
ALIYUN_OSS_BUCKET = os.getenv("ALIYUN_OSS_BUCKET")


assert ALIYUN_OSS_AK
assert ALIYUN_OSS_PK
