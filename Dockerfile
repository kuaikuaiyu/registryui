FROM python:2.7
COPY ./ /code
WORKDIR /code

ENV LOG_LEVEL="info"
ENV REDIRECT_URL="https://kuaikuaiyu.com"
ENV SERVER_PORT="8080"
ENV SERVER_ADDRESS="0.0.0.0"
ENV ALIYUN_OSS_AK=""
ENV ALIYUN_OSS_PK=""
ENV ALIYUN_OSS_ENDPOINT=""
ENV ALIYUN_OSS_BUCKET=""

RUN pip install -r requirements.txt
CMD ["python", "app.py"]
