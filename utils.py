#!/usr/bin/env python
# -*- coding: utf8 -*-
from tornado import gen

def async_map(fn, iterable, callback):
    result = []
    def save_result(value):
        result.append(value)
        if len(result) == len(iterable):
            callback(result)
    for i in iterable:
        fn(i, save_result)