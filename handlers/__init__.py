#!/usr/bin/env python
# -*- coding: utf8 -*-
import tornado.web
from tornado import gen
import registry
import logging
import json


class IndexHandler(tornado.web.RequestHandler):
    @gen.coroutine
    def get(self, *args, **kwargs):
        infos = yield registry.fetch_registry_info()
        self.render('index.html', infos=infos)



class JSONHandler(tornado.web.RequestHandler):
    @gen.coroutine
    def get(self, *args, **kwargs):
        infos = yield registry.fetch_registry_info()
        self.write(json.dumps(infos))
