#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
import logging
import tornado.web
from tornado.ioloop import IOLoop

from configure import REDIRECT_URL
from configure import SERVER_ADDRESS, SERVER_PORT
from configure import DEBUG
from configure import TEMPLATE_PATH
from configure import LOG_LEVEL
from configure import LOG_FORMAT
from handlers import IndexHandler
from handlers import JSONHandler


from tornado.httpclient import AsyncHTTPClient
# tornado will reuse AsyncHTTPClient instance, set max_clients large enough to speedup
AsyncHTTPClient(max_clients=128)

logging.basicConfig(level=LOG_LEVEL, format=LOG_FORMAT)
URLs = [
    (r'/', IndexHandler),
    (r'/json', JSONHandler),
    (r'/.*$', tornado.web.RedirectHandler, {'url': REDIRECT_URL}),
]

if __name__ == "__main__":
    application = tornado.web.Application(URLs, debug=DEBUG, template_path=TEMPLATE_PATH)
    application.listen(SERVER_PORT, SERVER_ADDRESS, xheaders=True)
    logging.info("server is running on %s:%d", SERVER_ADDRESS, SERVER_PORT)
    loop = IOLoop.instance()
    loop.start()
